import os
import sys
from subprocess import call, STDOUT

TEMPLATE_FOLDER = 'templates'

if len(sys.argv) != 2:
    print("ERROR: No filename specified")
    print("Usage: python convert.py %filename%")
    print("E.g.:  python convert.py example_article.txt")
    quit()
filename = sys.argv[1]

print('Reading [%s]' % filename)
expert_dict={}
for l in open(filename, 'r'):
    if l.startswith('#'):
        continue
    if '=' not in l:
        print("Line without '=' found, aborting...")
        break
    key, value = [x.strip() for x in l.split('=')]
    expert_dict[key] = value

texlog_file = 'texlog'
if os.path.exists(texlog_file):
    print("Deleting [%s]" % texlog_file)
    os.remove(texlog_file)

def create_pdf(template, tmp_tex):
    print('Creating temporary file [%s]' % tmp_tex)
    with open(tmp_tex,'w') as f:
        for l in open(template, 'r'):
            for k in expert_dict:
                l = l.replace('TMP-%s' % k, expert_dict[k])
            f.write(l)

    print('Compiling [%s]...' % tmp_tex)

    with open('texlog', 'a') as f:
        call(['xelatex', '-interaction=nonstopmode', tmp_tex], stdout=f, stderr=STDOUT)
    
    filename = tmp_tex[:-4]
    files_to_remove = [tmp_tex, '%s.aux' % filename, '%s.log' % filename]
    
    for f in files_to_remove:
        if os.path.exists(f):
            os.remove(f)
        else:
            print("WARNING: File [%s] not created, check [%s]" % (f, texlog_file))
    
    pdf_file = "%s.pdf" % filename
    if not os.path.exists(pdf_file):
        print("ERROR: PDF file [%s] wasn't created, check [%s]" % (pdf_file,texlog_file))
    else:
        print("PDF file [%s] created" % pdf_file)

create_pdf('templates/napr.tex', 'napr.tex')
create_pdf('templates/vyvos.tex', 'vyvos.tex')
create_pdf('templates/eksp.tex', 'eksp.tex')
